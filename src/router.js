import VueRouter from "vue-router";

const Soundboard = () => import(/* webpackChunkName: "soundboard" */ './pages/Soundboard');
const Karaoke = () => import(/* webpackChunkName: "karaoke" */ './pages/Karaoke');
const KaraokeStats = () => import(/* webpackChunkName: "karaoke" */ './pages/KaraokeStats');
const Songs = () => import(/* webpackChunkName: "songs" */ './pages/Songs');
const About = () => import(/* webpackChunkName: "about" */ './pages/About');

export default new VueRouter({
    root: '/ia',
    mode: 'history',
    routes: [
        {
            path: '/ia/soundboard',
            component: Soundboard,
        },
        {
            path: '/ia/karaoke',
            component: Karaoke,
        },
        {
            path: '/ia/karaoke/stats',
            component: KaraokeStats,
        },
        {
            path: '/ia/songs',
            component: Songs,
        },
        {
            path: '/ia/about',
            component: About,
        },
        {
            path: '*',
            redirect: '/ia/soundboard',
        },
    ],
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }

        return { x: 0, y: 0 };
    },
});
