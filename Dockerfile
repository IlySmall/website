FROM node:16 AS build

COPY . /src

WORKDIR /src

RUN npm ci && \
    npm run build

FROM nginx:1 AS web

COPY .nginx.conf /etc/nginx/templates/default.conf.template

COPY --from=build /src/dist/ /app
